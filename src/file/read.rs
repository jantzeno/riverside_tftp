use std::collections::HashMap;
use std::fs::File;
use std::io::{BufReader, Read};
use std::path::PathBuf;

pub struct ReadManager {
    files: HashMap<u16, BufReader<File>>,
    filenames: HashMap<u16, String>,
}

impl ReadManager {
    pub fn new() -> Self {
        Self {
            files: HashMap::new(),
            filenames: HashMap::new(),
        }
    }

    pub fn open(&mut self, id: u16, path: &PathBuf) -> Result<(), std::io::Error> {
        match validate_path(path) {
            Ok(_) => (),
            Err(err) => return Err(err),
        }

        let file = File::open(path.as_path())?;
        let filename = String::from(path.file_name().unwrap().to_str().unwrap());

        let reader = BufReader::new(file);

        self.files.insert(id, reader);
        self.filenames.insert(id, filename);
        Ok(())
    }

    pub fn has_file(&self, id: u16) -> bool {
        !self.files.is_empty() && self.files.contains_key(&id)
    }

    pub fn has_filename(&self, id: u16) -> bool {
        !self.filenames.is_empty() && self.filenames.contains_key(&id)
    }

    pub fn get_file(&mut self, id: u16) -> Option<BufReader<File>> {
        self.files.remove(&id)
    }

    pub fn return_file(&mut self, id: u16, reader: BufReader<File>) -> Option<BufReader<File>> {
        self.files.insert(id, reader)
    }

    pub fn get_filename(&self, id: u16) -> Result<&String, std::io::Error> {
        match self.filenames.get(&id) {
            Some(filename) => Ok(filename),
            None => Err(std::io::Error::new(
                std::io::ErrorKind::NotFound,
                format!("no filename for id: {}", id),
            )),
        }
    }

    pub fn remove_file(&mut self, id: u16) -> Result<(), std::io::Error> {
        // Remove from files
        if let None = self.files.remove(&id) {
            return Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                format!("no file for id: {}", id),
            ));
        }

        // Remove from filenames
        if let None = self.filenames.remove(&id) {
            return Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                format!("no filename for id: {}", id),
            ));
        }

        Ok(())
    }
}

fn validate_path(path: &PathBuf) -> Result<(), std::io::Error> {
    if !path.exists() {
        return Err(std::io::Error::new(
            std::io::ErrorKind::NotFound,
            format!("{} does not exist", path.display()),
        ));
    }
    if !path.is_file() {
        return Err(std::io::Error::new(
            std::io::ErrorKind::InvalidInput,
            format!("{} is not a file", path.display()),
        ));
    }
    Ok(())
}

pub struct ReadResult {
    pub reader: BufReader<File>,
    pub buffer: [u8; 512],
    pub size: usize,
}

pub fn read_file_buffer(mut reader: BufReader<File>) -> Result<ReadResult, std::io::Error> {
    let mut buffer = [0; 512];
    match reader.read(&mut buffer) {
        Ok(len) => Ok(ReadResult {
            reader,
            buffer,
            size: len,
        }),
        Err(err) => Err(err),
    }
}

#[cfg(test)]
mod tests {

    use crate::file::read as rs_read;
    use std::{
        fs::File,
        io::{BufReader, Read},
    };

    #[test]
    fn test_read_manager_valid_path_and_id() {
        let file_path = &std::path::PathBuf::from("./test_data/2br02b.txt");
        let mut read_manger = rs_read::ReadManager::new();
        let id = 1;

        // Test Valid
        // Create file manager, open file, check for file properties
        assert!(read_manger.open(id, file_path).is_ok());
        assert_eq!(read_manger.has_file(id), true);
        assert_eq!(read_manger.has_filename(id), true);
        assert_eq!(read_manger.get_filename(id).unwrap(), "2br02b.txt");

        // Create a reader and read from file, then return to file manager
        let mut reader = read_manger.get_file(id).unwrap();
        assert_eq!(reader.read(&mut [0; 512]).unwrap(), 512);

        // Return to file manager and remove file
        assert!(read_manger.return_file(id, reader).is_none());
        assert!(read_manger.remove_file(id).is_ok());
    }

    #[test]
    fn test_read_manager_invalid_id() {
        let file_path = &std::path::PathBuf::from("./test_data/2br02b.txt");
        let mut read_manager = rs_read::ReadManager::new();
        let invalid_id = 2;

        // Test Invalid
        assert_eq!(read_manager.has_file(invalid_id), false);
        assert_eq!(read_manager.has_filename(invalid_id), false);
        assert!(read_manager.get_filename(invalid_id).is_err());
        assert!(read_manager.get_file(invalid_id).is_none());
        let reader = BufReader::new(File::open(file_path.as_path()).unwrap());
        assert!(read_manager.return_file(invalid_id, reader).is_none());
        assert!(read_manager.remove_file(invalid_id).is_err());
    }

    #[test]
    fn test_file_manager_invalid_file_path() {
        let file_path = &std::path::PathBuf::from("./test_data/no_file.txt");
        let mut read_manager = rs_read::ReadManager::new();
        let id = 1;

        assert!(read_manager.open(id, file_path).is_err());
    }

    #[test]
    fn test_validate_path() {
        // Valid path
        let path = &std::path::PathBuf::from("./test_data/2br02b.txt");
        assert!(rs_read::validate_path(&path).is_ok());
        // Invalid path
        let path = &std::path::PathBuf::from("./test_data/no_file.txt");
        assert!(rs_read::validate_path(&path).is_err());
    }

    #[test]
    fn test_read_file_buffer_valid_file_manager() {
        let file_path = &std::path::PathBuf::from("./test_data/2br02b.txt");
        let mut read_manager = rs_read::ReadManager::new();
        let id = 1;

        assert!(read_manager.open(id, file_path).is_ok());
        // Valid tests
        // Specific to testdata/2br02b.txt
        // File has 28 blocks of [u8, 512] and 1 block of [u8, 426]
        for buffer_count in 0..30 {
            let reader = read_manager.get_file(id).unwrap();
            let read = rs_read::read_file_buffer(reader).unwrap();
            if buffer_count < 28 {
                assert_eq!(read.size, 512);
                assert_eq!(read.buffer.len(), read.size);
            } else if buffer_count == 28 {
                assert_eq!(read.size, 426);
                assert!(read.size < 512);
            } else if buffer_count == 29 {
                assert_eq!(read.size, 0);
            } else {
                assert!(false);
            }
            assert!(read_manager.return_file(id, read.reader).is_none());
        }
    }
}
