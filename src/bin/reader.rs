/*
riverside_tftp
Copyright (C) 2023  Jantzen Owens

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

use riverside_tftp::file::read::{self as rs_read};

fn main() -> std::io::Result<()> {
    let path = std::env::current_dir()?;
    println!("The current directory is {}", path.display());

    let mut file_manager = rs_read::ReadManager::new();
    let id = 1;

    match file_manager.open(id, &std::path::PathBuf::from("./test_data/2br02b.txt")) {
        Ok(_) => (),
        Err(err) => println!("{:?}", err),
    }

    loop {
        let reader = match file_manager.get_file(id) {
            Some(reader) => reader,
            None => {
                println!("nothing to read");
                break;
            }
        };

        match rs_read::read_file_buffer(reader)? {
            rs_read::ReadResult {
                reader,
                buffer: _,
                size: 0,
            } => {
                file_manager.return_file(id, reader);
                break;
            }
            rs_read::ReadResult {
                reader,
                buffer: _,
                size: 512,
            } => {
                file_manager.return_file(id, reader);
            }
            rs_read::ReadResult {
                reader,
                buffer: _,
                size: _,
            } => {
                file_manager.return_file(id, reader);
            }
        }
    }
    Ok(())
}
