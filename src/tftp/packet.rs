#![allow(dead_code)]
#![allow(unused_variables)]
enum Mode {
    Netascii,
    Octet,
    Mail, // error, unimplemented
}

impl Mode {
    fn value(&self) -> &[u8] {
        match *self {
            Mode::Netascii => b"netascii",
            Mode::Octet => b"octet",
            Mode::Mail => b"mail", // error, unimplemented
        }
    }
}

enum RequestType {
    RRQ = 1,
    WRQ = 2,
}

#[repr(u16)]
enum OperationType {
    Request(RequestType),
    Data = 3,
    Ack = 4,
    Error = 5,
}

const END_BYTE: u8 = 0;

struct RqPacket {
    opcode: RequestType,
    filename: String,
    mode: Mode,
}

struct DataPacket {
    opcode: u16,
    block_id: u16,
    data: [u8; 512],
}

impl DataPacket {
    fn opcode(&self) -> OperationType {
        OperationType::Data
    }
}

struct AckPacket {
    opcode: u16,
    block_id: u16,
}

impl AckPacket {
    fn opcode(&self) -> OperationType {
        OperationType::Ack
    }
}

enum ErrorCode {
    NotDefined,
    FileNotFound,
    AccessViolation,
    DiskFull,
    IllegalOpertation,
    UnknownTransferId,
    FileAlreadyExists,
    NoSuchUser,
}

impl ErrorCode {
    fn opcode(&self) -> OperationType {
        OperationType::Error
    }

    fn value(&self) -> u16 {
        match *self {
            ErrorCode::NotDefined => 0,
            ErrorCode::FileNotFound => 1,
            ErrorCode::AccessViolation => 2,
            ErrorCode::DiskFull => 3,
            ErrorCode::IllegalOpertation => 4,
            ErrorCode::UnknownTransferId => 5,
            ErrorCode::FileAlreadyExists => 6,
            ErrorCode::NoSuchUser => 7,
        }
    }

    fn meaning(&self) -> &[u8] {
        match *self {
            ErrorCode::NotDefined => b"not defined",
            ErrorCode::FileNotFound => b"file not found",
            ErrorCode::AccessViolation => b"access violation",
            ErrorCode::DiskFull => b"disk full or allocation exceeded",
            ErrorCode::IllegalOpertation => b"illegal tftp operation",
            ErrorCode::UnknownTransferId => b"unknown transfer id",
            ErrorCode::FileAlreadyExists => b"file already exists",
            ErrorCode::NoSuchUser => b"no such user",
        }
    }
}

struct ErrorPacket {
    opcode: u16,
    error_code: u16,
    err_msg: String,
}

fn zero_terminate_string(buffer: &str) -> Result<&str, ()> {
    let bytes = buffer.as_bytes();

    Result::Ok("ok")
}
