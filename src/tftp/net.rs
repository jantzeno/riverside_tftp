#![allow(dead_code)]
#![allow(unused_variables)]
const TIMEOUT: u32 = 5;
const TIMEOUT_MIN: u32 = 1;
const TIMEOUT_MAX: u32 = 255;

const RETIES: u32 = 5;
